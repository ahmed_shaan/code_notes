<?php

//database class
class Dbh {
    private $host = "localhost";
    private $user = "root";
    private $pwd = "";
    private $dbName = "oopphp16";

    protected function connect() {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($dsn, $this->user, $this->pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}



// if using external file db class please include it first. in this example, dnt need to import.

class Test extends Dbh{

    //to get users data
    public function getUsers(){
        $sql = "SELECT * FROM users";
        $stmt = $this->connect()->query($sql);
        while ($row = $stmt->fetch()){
            echo $row['users_firstname'] . '</br> ';
        }
    }

    //above function implemented with prepared statements
    public function getUsersStmt($firstname, $lastname){
        $sql = "SELECT * FROM users WHERE users_firstname = ? AND users_lastname = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$firstname, $lastname]);
        $names = $stmt->fetchAll();

        foreach($names as $name){
            echo $name['users_firstname'] . '</br> ';
        }
    }

    //to insert users data
    public function setUsersStmt($firstname, $lastname, $dob){
        $sql = "INSERT INTO users(users_firstname, users_lastname, users_dateofbirth) VALUES (?,?,?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$firstname, $lastname, $dob]);
       
    }


}






//get users in index.php

$testObj = new Test();
echo $testObj->getUsers();

//to get users with prepared statement in index.php
$testObj = new Test();
echo $testObj->getUsersStmt("Daniel", "Neilsen");

//to insert users with prepared statement in index.php
$testObj = new Test();
echo $testObj->setUsersStmt("Daniel", "Neilsen","2019-03-01");

<?php


//classes and objects
//===================




//abstract class by mmtuts

abstract class Visa {
    public function visaPayment(){
        return "Perform a payment";
    }

    abstract public function getPayment(); 
}


class BuyProduct extends Visa {
    public function getPayment()
    {
        return $this->visaPayment();
    }
}



$buyProduct = new BuyProduct();
echo $buyProduct->getPayment();




//abstract by bitfumes

abstract class Car{
    function getName(){
        return 'I m a car';
    }

    abstract public function calculateTankArea();
}



class BMW extends Car{
    public function calculateTankArea()
    {
        
    }
}


//====================================================



//interfaces by mmtuts

interface PaymentInterface {
    public function payNow();
}

interface LoginInterface {
    public function loginFirst();
}



class Paypal implements PaymentInterface, LoginInterface  {
    public function loginFirst(){}
    public function payNow(){}
    public function paymentProcess(){
        $this->loginFirst();
        $this->payNow();
    }
}

class BankTransfer implements PaymentInterface, LoginInterface  {
    public function loginFirst(){}
    public function payNow(){}
    public function paymentProcess(){
        $this->loginFirst();
        $this->payNow();
    }
}


class Visa implements PaymentInterface, LoginInterface {
    public function payNow(){}
    public function paymentProcess(){
        $this->loginFirst();
        $this->payNow();
    }
}

class Cash implements PaymentInterface, LoginInterface  {
    public function payNow(){}
    public function paymentProcess(){
        $this->loginFirst();
        $this->payNow();
    }
}


class BuyProduct {
    public function pay(PaymentInterface $paymentType) {
        $paymentType->paymentProcess();
    }

    public function onlinePay(LoginInterface $paymentType) {
        $paymentType->paymentProcess();
    }
}


$paymentType = new Cash();
$buyProduct = new BuyProduct();
$buyProduct->pay($paymentType);





/*The Abstract Class
An abstract class is a class that not only has the ability 
to define required methods for concrete classes, but can also
provide an implementation of those methods. To use an abstract
class, a concrete class must extend the abstract class which 
then makes all of the methods defined in the abstract available 
inside the concrete class.*/


abstract class Data_Access {

    protected function dbConnect() {
        
        $dbHost = 'localhost';
        $dbUsername = 'root';
        $dbPassword = 'root';
        $dbSchema = 'classicmodels';

        //establish a database connection
        if (!isset($GLOBALS['dbConnection'])) {
            $GLOBALS['dbConnection'] = new mysqli($dbHost, $dbUsername, $dbPassword, $dbSchema);
        }

        //if an error occured, record it
        if (mysqli_connect_errno()) {
            // if an error occured, raise it
            $responseArray['response'] = '500';
            $responseArray['message'] = 'MySQL error: ' . mysqli_connect_errno() . ' ' . mysqli_connect_error();
        } else {
            //success
            $responseArray['response'] = '200';
            $responseArray['message'] = 'Database connection successful.';
        }

        return $responseArray;

    }


    protected function getResultSetArray($varQuery) {
        
        // attempt the query
        $rsData = $GLOBALS['dbConnection']->query($varQuery);

        //if and error occurred, raise it
        if (isset($GLOBALS['dbConnection']->errno) && ($GLOBALS['dbConnection']->errno !=0)) {
            //if an error occured , raise it
            $responseArray['response'] = '500';
            $responseArray['message'] = 'Internal server error. MySQL error: ' . $GLOBALS['dbConnection']->errno;
        } else {
            //success
            $rowCount = $rsData->num_rows;

            if($rowCount !=0) {
                // move result set to an associative array
                $rsArray = $rsData->fetch_all(MYSQLI_ASSOC);

                // add array to return
                $responseArray['response'] = '200';
                $responseArray['message'] = 'Success';
                $responseArray['dataArray'] = $rsArray;

                // Free result set
                $rsData->free_result();

            } else {
                //no data returned
                $responseArray['response'] = '204';
                $responseArray['message'] = 'Query did not return any results.';
            }

        }
        return $responseArray;
    }
}